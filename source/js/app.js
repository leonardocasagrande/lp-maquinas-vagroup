$(".menu").on("click", function () {
  $(".menu-window").toggleClass("opened");
});

$(".clickBtn").on("click", function (e) {
  e.preventDefault();
  $(".menu-window").toggleClass("opened");
  $(".menu").toggleClass("opened");

  $("html,body").animate(
    {
      scrollTop: eval($("#" + $(this).attr("target")).offset().top - 80),
    },
    100
  );
});

$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});

$("input[name=email-desktop]", "input[name=email-mobile]").attr(
  "onblur",
  "validateEmail(this)"
);

function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}
$(document).ready(function(){
  
  $.ajax({
    url: 'https://www.valgroupco.com/wp-json/wp/v2/posts?_embed',
    type: 'GET',
    data:{
      per_page: 3,
      categories: 170
    },
    dataType: 'json',

    statusCode:{
      200: function(data, status, xhr){
        
        var content;
        if(data.length){
          content =`
            ${data.map(element=> `
              <div class="col-lg-4 my-4">
                <div class="item-blog">
                  <div class="img-blog" style="background-image:url(${element.featured_image_src})"></div>
                  <div class="corpo-blog">
                    <h3 class="titulo-blog">${element.title.rendered}</h3>
                    <p class="resumo-blog">${element.excerpt.rendered}</p>
                    <a target="_blank" href="${element.link}" class="link-blog">LEIA MAIS <svg xmlns="http://www.w3.org/2000/svg" width="13.513" height="8.278" viewBox="0 0 13.513 8.278"><path id="seta" d="M261.8,266.354a.7.7,0,0,0,0-.528.679.679,0,0,0-.151-.225l-3.447-3.447a.691.691,0,0,0-.977.977l2.268,2.268H249.029a.691.691,0,0,0,0,1.382h10.463l-2.268,2.268a.691.691,0,0,0,.977.977l3.447-3.447A.694.694,0,0,0,261.8,266.354Z" transform="translate(-248.338 -261.95)" fill="#2d8a7a"/></svg></a>
                  </div>
                </div>
              </div>
            `).join('')}
          `;
          $("#list-blog").append(content);
        }
      }
    }
  })
})