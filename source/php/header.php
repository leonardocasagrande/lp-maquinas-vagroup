<!DOCTYPE html>

<html lang="pt_BR">

<head>

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>

    <?php wp_title(); ?>

  </title>

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">

</head>

<body>

  <section class="mobile-menu d-lg-none">

    <div class="container">

      <a href="<?= get_site_translated(); ?>">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header.png" alt="Valgroup">
      </a>

      <button class="menu" onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))" aria-label="Main Menu">
        <svg width="40" height="40" viewBox="0 0 100 100">
          <path class="line line1" d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
          <path class="line line2" d="M 20,50 H 80" />
          <path class="line line3" d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
        </svg>
      </button>

    </div>

    <div class="menu-window">


      <div class="links">




        <a target="maquina-semi-automatica" class="clickBtn link">
          <?= __("[:pt] Máquinas </br>semi-automáticas [:en]Semiautomatic  </br>Machines [:es]Máquinas    </br>Semiautomáticas ") ?>
        </a>

        <a target="maquina-automatica" class="clickBtn link">
          <?= __("[:pt] Máquinas automáticas[:en]Automatic Machines [:es]Máquinas Automáticas") ?>
        </a>

        <a target="suporte-tecnico" class="clickBtn link"> <?= __("[:pt] Suporte Técnico[:en]Technical Support [:es]Soporte Técnico") ?></a>

        <a target="vendas-e-comodatos" class="clickBtn link">
          <?= __("[:pt] Vendas e Comodatos[:en]Sales and Commodates [:es]Ventas y Comodatos  ") ?>
        </a>

        
        <a target="blog" class="clickBtn link">
          <?= __("[:pt] blog[:en]blog [:es]blog  ") ?>
        </a>
        <?php wpm_language_switcher("list", "flag"); ?>
      </div>


      <div class="down-btns">
        <a target="contato" class="clickBtn contact">
          <?= __("[:pt] Entre em contato[:en]Contact Us [:es]Entre en contacto  ") ?>
        </a>

        <a href="https://www.valgroupco.com" class="visit">
          <?= __("[:pt] Visite nosso site [:en]Visit our website [:es]Visite nuestro sitio web ") ?>
          <i class="fa-solid fa-arrow-right"></i></a>
      </div>

    </div>


  </section>

  <section class="desk-menu d-none d-lg-block">

    <div class="container d-lg-flex align-items-center justify-content-between">
      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header.png" alt="">

      <?php wpm_language_switcher("list", "flag"); ?>


      <div class="links">
        <a target="maquina-semi-automatica" class="clickBtn link">
          <?= __("[:pt] Máquinas </br>semi-automáticas [:en]Semiautomatic  </br>Machines [:es]Máquinas    </br>Semiautomáticas ") ?>
        </a>

        <a target="maquina-automatica" class="clickBtn link">
          <?= __("[:pt] Máquinas </br>automáticas[:en]Automatic </br>Machines [:es]Máquinas </br>Automáticas") ?>
        </a>

        <a target="suporte-tecnico" class="clickBtn link"> <?= __("[:pt] Suporte </br>Técnico[:en]Technical </br>Support [:es]Soporte </br>Técnico") ?></a>

        <a target="vendas-e-comodatos" class="clickBtn link">
          <?= __("[:pt] Vendas e </br>Comodatos[:en]Sales and </br>Commodates [:es]Ventas y </br>Comodatos  ") ?>
        </a>
        <a target="blog" class="clickBtn link">
          <?= __("[:pt] Blog[:en]Blog [:es]Blog  ") ?>
        </a>
        <a target="contato" class="clickBtn contact">
          <?= __("[:pt] Entre em contato[:en]Contact Us [:es]Entre en contacto  ") ?>
        </a>
      </div>
    </div>

  </section>