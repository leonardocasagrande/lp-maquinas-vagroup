<?php

add_filter('show_admin_bar', '__return_false');


function get_site_translated(){
	$site_url = get_site_url();
	$lingua = get_locale();
	if($lingua =="es_ES"){
		return $site_url."/es";
	}elseif($lingua =="en_US"){
		return $site_url."/en";
	}else{
		return $site_url;
	}
	
}

?>