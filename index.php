<?php get_header();

$lang = get_locale();
?> <section class="intro" id="topo"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pattern.png" alt="" class="d-none d-lg-block detail"><div class="container"><div class="forza-tech d-none d-lg-block"><span> <?= __("[:pt] tecnologia exclusiva[:en]Exclusive technology [:es]Tecnología exclusiva") ?> </span><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/FORZA.png" alt=""></div><div class="d-lg-flex align-items-center"><img class="img-fluid banner-img p-lg-0 pb-4 px-0" src="<?= the_field('banner'); ?>" alt="máquinas valgroup"><div class="col-lg-4 px-0"><h2 class="titulo"><?= the_field('titulo_banner'); ?></h2> <?php

        // Check rows exists.
        if (have_rows('repetidor_intro')) :
        ?> <div class="qualidades"> <?php

            // Check rows exists.
            while (have_rows('repetidor_intro')) : the_row();
              $icon = get_sub_field('icon');
              $titulo = get_sub_field('titulo');
            ?> <div class="item"><div class="circle"><img class="img-fluid" src="<?= $icon; ?>" alt=""></div><h3><?= $titulo; ?></h3></div> <?php endwhile; ?> <a target="maquinas" class="clickBtn btn-gradient"><?= __("[:pt] saiba mais[:en]know more [:es]sepa más") ?></a></div> <?php endif; ?> </div></div><div id="maquinas" class="maquinas-valgroup d-lg-flex flex-row-reverse"><div class="col-lg-6"><h3 class="titulo-mid"><?= the_field('titulo_maquinas'); ?></h3> <?= the_field('texto_maquinas'); ?> <a href="https://valgroupco.com/tecnologia-e-inovacao/ltc" target="_blank" class="ver-ltc d-none d-lg-block"><i class="fa-solid fa-circle-play mr-1"></i> <?= __("[:pt] saiba mais sobre o ltc[:en]Learn more about LTC[:es]Sepa más sobre el LTC") ?> </a></div><div class="wrapper-video"><img class="img-fluid ltc" src="<?= the_field('imagem_ltc'); ?>" alt=""> <img class="img-fluid thumb d-none d-lg-block" src="<?= the_field('thumn_video'); ?>" alt=""> <button type="button" class="video-open d-lg-block d-none" data-bs-toggle="modal" data-bs-target="#exampleModal"><span class="play-hover"><i class="fa-solid fa-circle-play mr-1"></i> <span class="text"> <?= __("[:pt] ASSISTIR AO VÍDEO[:en]WATCH THE VIDEO[:es]VER EL VÍDEO") ?> </span></span></button></div></div></div><button type="button" class="video-open d-lg-none" data-bs-toggle="modal" data-bs-target="#exampleModal"><img class="img-fluid" src="<?= the_field('thumn_video'); ?>" alt=""></button><div class="wrapper-line"><div class="container border-bt-custom"> <?php

      // Check rows exists.
      if (have_rows('repetidor_qualidades')) :
      ?> <div class="after-video-wrapper pt-lg-0 d-lg-flex align-items-end justify-content-between"> <?php

          // Check rows exists.
          while (have_rows('repetidor_qualidades')) : the_row();
            $icon = get_sub_field('icon');
            $titulo = get_sub_field('titulo');
            $texto = get_sub_field('texto');
          ?> <div class="after-video-item"><div class="circle"><img class="img-fluid" src="<?= $icon; ?>" alt="" class="icon"></div><h4 class="titulo-mid"><?= $titulo; ?></h4><p><?= $texto; ?></p></div> <?php endwhile; ?> </div> <?php endif; ?> </div></div><div class="wrapper-line"><div class="forza-section border-bt-custom d-lg-flex align-items-center justify-content-between container"><img class="img-fluid" src="<?= the_field('forza_imagem'); ?>" alt=""> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-forza.png" alt="" class="d-none d-lg-block detail-forza"><div class="col-lg-6 px-0"><h3 class="titulo-mid"><?= the_field('forza_titulo'); ?></h3> <?= the_field('forza_texto'); ?> </div></div></div></section> <?php
if (have_rows('repetidor_intro')) :
?> <section id="maquina-semi-automatica" class="maquinas"><div class="container maquina"><h2 class="titulo-mid"> <?= __("[:pt] Máquinas envolvedoras semiautomáticas[:en]Semi-automatic packaging machines[:es]Máquinas envolvedoras semiautomáticas") ?> </h2><div class="pane"><div class="wrapper-lg d-lg-flex justify-content-between align-items-center"><div class="line"></div><span class="sub"> <?= __("[:pt] Selecione a máquina ideal[:en]Select the ideal machine[:es]Seleccione la máquina ideal") ?> </span><ul class="nav nav-pills mb-3" id="pills-tab" role="tablist"> <?php while (have_rows('repetidor_semiautomaticas')) : the_row();
              $titulo = get_sub_field('titulo');
              $texto = get_sub_field('texto');
              $index = get_row_index(); ?> <li class="nav-item" role="presentation"><button class="nav-link <?php if ($index === 1) : echo 'active';
                                        endif; ?>" id="pills-home-tab-<?= $index; ?>" data-bs-toggle="pill" data-bs-target="#pills-home-<?= $index; ?>" type="button" role="tab" aria-controls="pills-home-<?= $index; ?>" aria-selected="true"><?= $titulo; ?></button></li> <?php endwhile; ?> </ul><div class="line"></div></div><div class="tab-content" id="pills-tabContent"> <?php while (have_rows('repetidor_semiautomaticas')) : the_row();
            $titulo = get_sub_field('titulo');
            $texto = get_sub_field('texto');
            $tipo = get_sub_field('tipo');
            $imagem = get_sub_field('imagem');
            $index = get_row_index();


          ?> <div class="tab-pane fade <?php if ($index === 1) : echo 'show active';
                                        endif; ?>" id="pills-home-<?= $index; ?>" role="tabpanel" aria-labelledby="pills-home-tab-<?= $index; ?>"><div class="teste"> <?php if (!$tipo) : ?> <!-- <div class="img-wrapper col-lg-6 px-0"> --> <img class="img-fluid" src="<?= $imagem; ?>" alt=""><!-- </div> --> <?php endif; ?> <div class="text"> <?php if ($tipo) : ?> <div class="header-tab col-lg-8"><h4 class="titulo-mid"><?php if ($lang !== 'en_US') : echo "Máquina ";
                                              endif; ?><?= $titulo; ?> <?php if ($lang === 'en_US') : echo "Machine";
                                                                        endif; ?></h4><ul class="nav nav-tabs" id="myTab2" role="tablist"> <?php while (have_rows('tipo')) : the_row();
                          $nome = get_sub_field('nome');
                          $texto = get_sub_field('texto');
                          $index = get_row_index();
                          $mini = get_sub_field('mini'); ?> <li class="nav-item" role="presentation"><button class="nav-link <?php if ($index === 1) : echo ' active';
                                                    endif; ?>" id="home-tab-n-<?= $index; ?>" data-bs-toggle="tab" data-bs-target="#home-n-<?= $index; ?>" type="button" role="tab" aria-controls="home-n-<?= $index; ?>" aria-selected="true"><?= $nome; ?></button></li> <?php endwhile; ?> </ul></div><div class="tab-content" id="myTabContent2"> <?php while (have_rows('tipo')) : the_row();
                        $nome = get_sub_field('nome');
                        $texto = get_sub_field('texto');
                        $img_tipo = get_sub_field('img_tipo');
                        $index = get_row_index();
                        $mini = get_sub_field('mini'); ?> <div class="tab-pane fade <?php if ($index === 1) : echo 'show active';
                                                  endif; ?>" id="home-n-<?= $index; ?>" role="tabpanel" aria-labelledby="home-tab-n-<?= $index; ?>"><div class="teste"><!-- <div class="img-wrapper col-lg-6"> --> <img class="img-fluid" src="<?= $img_tipo; ?>" alt=""><!-- </div> --><div class="text"> <?= $texto; ?> <span class="mini"><?= $mini; ?></span></div></div></div> <?php endwhile; ?> </div> <?php else : ?> <h4 class="titulo-mid"><?php if ($lang !== 'en_US') : echo "Máquina ";
                                            endif; ?> <?= $titulo; ?> <?php if ($lang === 'en_US') : echo "Machine";
                                                                      endif; ?></h4> <?= $texto; ?> <?php endif; ?> </div></div></div> <?php endwhile; ?> </div></div></div><div class="nr-12"><div class="d-lg-flex"><div class="wrapper-all col-lg-4 px-0"> <?php if (have_rows('carrossel_nr12')) : ?> <div class="wrapper-nr"><div class="carousel-nr"> <?php while (have_rows('carrossel_nr12')) : the_row();
                  $img = get_sub_field('imagem');

                ?> <img class="img-fluid" src="<?= $img; ?>" alt=""> <?php endwhile; ?> </div></div> <?php endif; ?> </div><div class="text-nr col-lg-7"><h3 class="titulo-mid"><?= the_field('titulo_nr12'); ?><img class="img-fluid" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/nr-logo.png" alt=""></h3> <?= the_field('texto_nr12'); ?> </div></div></div></section> <?php endif; ?> <section id="maquina-automatica" class="maquinas"><div class="container maquina"><h2 class="titulo-mid"> <?= __("[:pt] Máquinas envolvedoras automáticas[:en]Automatic packaging machines[:es]Máquinas envolvedoras automáticas") ?> </h2><div class="pane"><div class="wrapper-lg custom-wrapper d-lg-flex justify-content-between align-items-center"><div class="line"></div><span class="sub"> <?= __("[:pt] Selecione a máquina ideal[:en]Select the ideal machine[:es]Seleccione la máquina ideal") ?></span><ul class="nav nav-pills mb-3" id="pills-tab" role="tablist"> <?php while (have_rows('repetidor_automaticas')) : the_row();
            $titulo = get_sub_field('titulo');
            $texto = get_sub_field('texto');
            $index = get_row_index(); ?> <li class="nav-item" role="presentation"><button class="nav-link <?php if ($index === 1) : echo 'active';
                                      endif; ?>" id="pills-auto-tab-<?= $index; ?>" data-bs-toggle="pill" data-bs-target="#pills-auto-<?= $index; ?>" type="button" role="tab" aria-controls="pills-auto-<?= $index; ?>" aria-selected="true"><?= $titulo; ?></button></li> <?php endwhile; ?> </ul><div class="line"></div></div><div class="tab-content" id="pills-tabContent"> <?php while (have_rows('repetidor_automaticas')) : the_row();
          $titulo = get_sub_field('titulo');
          $texto = get_sub_field('texto');
          $imagem = get_sub_field('imagem');
          $index = get_row_index();


        ?> <div class="tab-pane fade <?php if ($index === 1) : echo 'show active';
                                      endif; ?>" id="pills-auto-<?= $index; ?>" role="tabpanel" aria-labelledby="pills-auto-tab-<?= $index; ?>"><div class="teste"><img class="img-fluid" src="<?= $imagem; ?>" alt=""><div class="text"><h4 class="titulo-mid"><?php if ($lang !== 'en_US') : echo "Máquina ";
                                        endif; ?> <?= $titulo; ?> <?php if ($lang === 'en_US') : echo "Machine";
                                                                  endif; ?></h4> <?= $texto; ?> </div></div></div> <?php endwhile; ?> </div></div></div></section><section id="suporte-tecnico"><div class="container"><h2 class="titulo-mid"> <?= __("[:pt] Suporte Técnico<br> Especializado[:en]Expert <br>Technical Support[:es]Soporte Técnico<br> Especializado") ?> </h2><div class="row flex-row-reverse"><div class="col-lg-6"><img class="img-fluid d-lg-none mb-4" src="<?= the_field('img_mobile_suporte') ?>" alt=""> <img class="img-fluid d-none d-lg-block" src="<?= the_field('img_desktop_suporte') ?>" alt=""></div><div class="col-lg-6"><ul class="nav nav-tabs" id="myTab" role="tablist"> <?php while (have_rows('suportes')) : the_row();
            $titulo = get_sub_field('nome');
            $index = get_row_index();
          ?> <li class="nav-item" role="presentation"><button class="nav-link <?php if ($index === 1) : echo 'active';
                                      endif; ?>" id="suporte-<?= $index ?>-tab" data-bs-toggle="tab" data-bs-target="#suporte-<?= $index ?>" type="button" role="tab" aria-controls="suporte-<?= $index ?>" aria-selected="true"><?= $titulo ?></button></li> <?php endwhile; ?> </ul><div class="tab-content" id="myTabContent"> <?php while (have_rows('suportes')) : the_row();
            $conteudo = get_sub_field('descricao');
            $index = get_row_index();
          ?> <div class="tab-pane fade <?php if ($index === 1) : echo 'show active';
                                      endif; ?>" id="suporte-<?= $index ?>" role="tabpanel" aria-labelledby="suporte-<?= $index ?>-tab"> <?= $conteudo; ?> </div> <?php endwhile; ?> </div></div></div></div></section> <?php

// Check rows exists.
if (have_rows('repetidor_vcl')) :

?> <section class="vcl" id="vendas-e-comodatos"><img style="width: 100%;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pattern-vcl.png" alt="" class="pattern d-none d-lg-block"><div class="container d-lg-flex flex-row-reverse"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/maquina-vcl.png" alt="" class="d-lg-block d-none maquina-lg"><div class="vcl-wrapper col-lg-10 px-0 d-lg-flex flex-wrap align-items-flex-start justify-content-between"> <?php
        // Loop through rows.
        while (have_rows('repetidor_vcl')) : the_row();
          $icon = get_sub_field('icon');
          $titulo = get_sub_field('titulo');
          $texto = get_sub_field('texto');

        ?> <div class="item"><div class="header"><div class="circle"><img src="<?= $icon; ?>" alt=""></div><h4 class="titulo-mid"><?= $titulo; ?></h4></div> <?= $texto; ?> </div> <?php endwhile; ?> <a target="contato" class="clickBtn btn-contact d-none d-lg-block"> <?= __("[:pt] entre em contato[:en]Contact us[:es]Entre en contacto") ?> </a></div><a target="contato" class="clickBtn btn-contact d-lg-none"> <?= __("[:pt] entre em contato[:en]Contact us[:es]Entre en contacto") ?> </a></div></section> <?php endif; ?> <section id="blog" class="blog"><div class="container"><h2 class="titulo">Blog</h2><p class="desc">Confira nossos conteúdos exclusivos sobre máquinas envolvedoras, segurança de carga e aplicação eficiente de embalagens</p><div class="row" id="list-blog"></div><a href="https://www.valgroupco.com/category/maquinas/" target="_blank" rel="noopener noreferrer" class="btn-blog">CONFIRA NOSSO BLOG</a></div></section><section id="contato" class="contato"><div class="container"><h2 class="titulo"><?= the_field('titulo_contato'); ?></h2><div class="form"> <?= do_shortcode('[contact-form-7 id="97" title="Formulário de contato"]'); ?> </div></div></section> <?php get_footer(); ?>